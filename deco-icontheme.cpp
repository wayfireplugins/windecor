#include "deco-icontheme.hpp"
#include "INIReader.h"

#include <sys/stat.h>
#include <unistd.h>
#include <libgen.h>
#include <wayfire/config.h>

#include <filesystem>
#include <sstream>

std::string str_tolower( std::string s ) {
    std::transform(
        s.begin(), s.end(), s.begin(), []( unsigned char c ) {
            return std::tolower( c );
        }
    );

    return s;
}


bool exists( std::string path ) {
    struct stat statbuf;

    if ( stat( path.c_str(), &statbuf ) == 0 ) {
        if ( S_ISDIR( statbuf.st_mode ) ) {
            return (access( path.c_str(), R_OK | X_OK ) == 0);
        }

        else if ( S_ISREG( statbuf.st_mode ) ) {
            return (access( path.c_str(), R_OK ) == 0);
        }

        else {
            return false;
        }
    }

    else {
        return false;
    }
}


std::vector<std::string> getDesktops( std::string path ) {
    std::vector<std::string> desktops;

    if ( !exists( path ) ) {
        return desktops;
    }

    for (const auto& entry : std::filesystem::directory_iterator( path ) ) {
        if ( entry.is_regular_file() and entry.path().extension() == ".desktop" ) {
            desktops.push_back( entry.path() );
        }
    }

    return desktops;
}


wf::windecor::IconThemeManager::IconThemeManager( std::string iconTheme ) {
    setIconTheme( iconTheme );
}


void wf::windecor::IconThemeManager::setIconTheme( std::string iconThemeName ) {
    if ( mIconTheme == iconThemeName ) {
        return;
    }

    std::string home_local_share = getenv( "HOME" );

    home_local_share += "/.local/share/icons/";

    std::vector<std::string> icoDirs = {
        home_local_share,
        "/usr/local/share/icons/",
        "/usr/share/icons/",
    };

    /* We use hicolor as the theme if @iconThemeName is empty */
    if ( iconThemeName.empty() ) {
        mIconTheme = "/usr/share/icons/hicolor/";
    }

    else {
        for ( std::string dir: icoDirs ) {
            if ( exists( dir + iconThemeName ) ) {
                mIconTheme = dir + iconThemeName + "/";
                break;
            }
        }

        if ( mIconTheme.empty() ) {
            if ( exists( "/usr/share/icons/Adwaita" ) ) {
                mIconTheme = "/usr/share/icons/Adwaita/";
            }

            else if ( exists( "/usr/share/icons/breeze" ) ) {
                mIconTheme = "/usr/share/icons/breeze/";
            }

            else {
                mIconTheme = "/usr/share/icons/hicolor/";
            }
        }
    }

    /* Fallback themes */
    std::vector<std::string> themes = { mIconTheme };
    INIReader                iconTheme( mIconTheme + "index.theme" );

    for ( auto fbTh: iconTheme.GetList( "Icon Theme", "Inherits", ',' ) ) {
        for ( std::string dir: icoDirs ) {
            if ( exists( dir + fbTh ) ) {
                themes.push_back( dir + fbTh + "/" );
            }
        }
    }

    /* If hicolor is not in the list, add it */
    if ( !std::count( themes.begin(), themes.end(), "/usr/share/icons/hicolor/" ) ) {
        themes.push_back( "/usr/share/icons/hicolor/" );
    }

    /* Now get the list of theme dirs; clear the list first */
    themeDirs.clear();
    for ( auto theme: themes ) {
        INIReader iconTheme( theme + "index.theme" );
        for ( auto dir: iconTheme.GetList( "Icon Theme", "Directories", ',' ) ) {
            themeDirs.push_back( theme + dir + "/" );
        }
    }

    themeDirs.push_back( "/usr/share/pixmaps/" );
}


std::string wf::windecor::IconThemeManager::getThemeIcon( std::string iconName ) const {
    std::vector<std::string> paths;

    for ( auto themeDir: themeDirs ) {
        if ( exists( themeDir + iconName + ".svg" ) ) {
            paths.push_back( themeDir + iconName + ".svg" );
        }

        if ( exists( themeDir + iconName + ".png" ) ) {
            paths.push_back( themeDir + iconName + ".png" );
        }
    }

    if ( paths.empty() ) {
        return std::string();
    }

    std::vector<std::string> filtered;

    /* scalable */
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/scalable" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 512px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/512" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 256px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/256" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 128px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/128" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 96px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/96" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 64px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/64" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 48px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/48" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 36px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/36" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 32px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/32" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 24px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/24" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 22px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/22" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* 16px */
    filtered.clear();
    std::copy_if(
        paths.begin(), paths.end(), std::back_inserter( filtered ), [ = ] ( std::string path ) {
            return (path.find( "/16" ) != std::string::npos);
        }
    );

    if ( !filtered.empty() ) {
        return filtered.at( 0 );
    }

    /* Return the first in the list */
    if ( !paths.empty() ) {
        return paths.at( 0 );
    }

    return std::string();
}


std::string wf::windecor::IconThemeManager::iconPathForAppId( std::string mAppId ) const {
    std::string home_local_share = getenv( "HOME" );

    home_local_share += "/.local/share/applications/";

    std::vector<std::string> appDirs = {
        home_local_share,
        "/usr/local/share/applications/",
        "/usr/share/applications/",
    };

    std::string iconName;
    bool        found = false;

    for ( auto path: appDirs ) {
        if ( exists( path + mAppId + ".desktop" ) ) {
            INIReader desktop( path + mAppId + ".desktop" );
            iconName = desktop.Get( "Desktop Entry", "Icon", "" );
        }

        else if ( exists( path + str_tolower( mAppId ) + ".desktop" ) ) {
            INIReader desktop( path + str_tolower( mAppId ) + ".desktop" );
            iconName = desktop.Get( "Desktop Entry", "Icon", "" );
        }

        /** No icon specified: try else-where */
        if ( !iconName.empty() ) {
            found = true;
            break;
        }
    }

    if ( !found and workHard ) {
        /** Iterate through all the common plases fro desktop files */
        for ( auto path: appDirs ) {
            std::vector<std::string> desktops = getDesktops( path );

            /** Check the desktop files for a given path */
            for ( std::string dskf: desktops ) {
                INIReader desktop( dskf );

                /* Check the executable name */
                std::istringstream iss( desktop.Get( "Desktop Entry", "Exec", "abcd1234/" ) );
                std::string        exec;
                getline( iss, exec, ' ' );

                bool isPositive = (std::filesystem::path( exec ).filename() == mAppId);

                if ( !isPositive ) {
                    /* Check the name */
                    std::istringstream nss( desktop.Get( "Desktop Entry", "Name", "abcd1234/" ) );
                    std::string        name;
                    getline( nss, name, ' ' );

                    isPositive = (name == mAppId);
                }

                if ( !isPositive ) {
                    /* Check StartupWMClass - electron apps set this, Courtesy @wb9688 */
                    std::istringstream ess( desktop.Get( "Desktop Entry", "StartupWMClass", "abcd1234/" ) );
                    std::string        cls;
                    getline( ess, cls, ' ' );

                    isPositive = (cls == mAppId);
                }

                /** If the match was positive, extract the icon name */
                if ( isPositive ) {
                    iconName = desktop.Get( "Desktop Entry", "Icon", "" );

                    if ( !iconName.empty() ) {
                        found = true;
                        break;
                    }
                }
            }

            if ( found ) {
                break;
            }
        }
    }

    /* Fallback: use wayland */
    if ( iconName.empty() ) {
        iconName = "wayland";
    }

    /* In case a full path is specified */
    if ( (iconName.at( 0 ) == '/') and exists( iconName ) ) {
        return iconName;
    }

    std::vector<std::string> iconNames;

    iconNames.push_back( iconName );

    std::string iconNameL = iconName;

    transform( iconNameL.begin(), iconNameL.end(), iconNameL.begin(), ::tolower );
    iconNames.push_back( iconNameL );

    iconNames.push_back( mAppId );

    std::string mAppIdL = mAppId;

    transform( mAppIdL.begin(), mAppIdL.end(), mAppIdL.begin(), ::tolower );
    iconNames.push_back( mAppIdL );

    std::string iconPath;

    for ( auto name: iconNames ) {
        iconPath = getThemeIcon( name );
    }

    if ( iconPath.empty() ) {
        iconPath = INSTALL_PREFIX "/share/wayfire/windecor/executable.svg";
    }

    return iconPath;
}
