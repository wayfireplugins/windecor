#include "deco-theme.hpp"
#include <wayfire/core.hpp>
#include <wayfire/opengl.hpp>
#include <wayfire/config.h>
#include <map>
#include <librsvg/rsvg.h>
#include <pango/pango.h>
#include <pango/pangocairo.h>
#include <wayfire/plugins/common/cairo-util.hpp>

#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <bits/stdc++.h>

typedef struct {
    double x;
    double y;
    double width;
    double height;
} RectF;

void drawRoundedRectangle( cairo_t *cr, wf::geometry_t fullRect, int titlePos, int radius, double border ) {
    RectF borderRect{
        .x      = border / 2.0,
        .y      = border / 2.0,
        .width  = fullRect.width - border / 2.0,
        .height = fullRect.height - border / 2.0,
    };

    if ( titlePos == 1 ) {
        cairo_move_to( cr, borderRect.x + radius, borderRect.y + radius );

        /** Top left arc */
        cairo_new_sub_path( cr );
        cairo_arc( cr, borderRect.x + radius, borderRect.y + radius, radius, M_PI, 3.0 * M_PI / 2.0 );

        cairo_line_to( cr, borderRect.width,      borderRect.y );

        /** Right border */
        cairo_line_to( cr, borderRect.width,      borderRect.height );

        /** Bottom border */
        cairo_line_to( cr, borderRect.x + radius, borderRect.height );

        /** Bottom left arc */
        cairo_arc( cr, borderRect.x + radius, borderRect.height - radius, radius, M_PI / 2.0, M_PI );

        cairo_close_path( cr );
    }

    else if ( titlePos == 2 ) {
        cairo_move_to( cr, borderRect.width - radius, borderRect.y + radius );

        /** Top right arc */
        cairo_new_sub_path( cr );
        cairo_arc( cr, borderRect.width - radius, borderRect.y + radius, radius, 3.0 * M_PI / 2.0, 0 );

        /** Right border */
        cairo_line_to( cr, borderRect.width, borderRect.height );

        /** Bottom border */
        cairo_line_to( cr, borderRect.x,     borderRect.height );

        /** Left border */
        cairo_line_to( cr, borderRect.x,     radius );

        /** Top left arc */
        cairo_arc( cr, borderRect.x + radius, borderRect.y + radius, radius, M_PI, 3.0 * M_PI / 2.0 );

        cairo_close_path( cr );
    }

    else if ( titlePos == 3 ) {
        cairo_move_to( cr, borderRect.width - radius, borderRect.height - radius );

        /** Bottom right arc */
        cairo_new_sub_path( cr );
        cairo_arc( cr, borderRect.width - radius, borderRect.height - radius, radius, 0.0, M_PI / 2.0 );

        /** Bottom border */
        cairo_line_to( cr, borderRect.x,              borderRect.height );

        /** Left border */
        cairo_line_to( cr, borderRect.x,              borderRect.y );

        /** Top border */
        cairo_line_to( cr, borderRect.width - radius, borderRect.y );

        /** Top right arc */
        cairo_arc( cr, borderRect.width - radius, borderRect.y + radius, radius, 3.0 * M_PI / 2.0, 0 );

        cairo_close_path( cr );
    }

    else if ( titlePos == 4 ) {
        cairo_move_to( cr, borderRect.x + radius, borderRect.height - radius );

        /** Bottom left arc */
        cairo_new_sub_path( cr );
        cairo_arc( cr, borderRect.x + radius, borderRect.height - radius, radius, M_PI / 2.0, M_PI );

        /** Left border */
        cairo_line_to( cr, borderRect.x,     borderRect.y );

        /** Top border */
        cairo_line_to( cr, borderRect.width, borderRect.y );

        /** Right border */
        cairo_line_to( cr, borderRect.width, borderRect.height - radius );

        /** Bottom right arc */
        cairo_arc( cr, borderRect.width - radius, borderRect.height - radius, radius, 0.0, M_PI / 2.0 );

        cairo_close_path( cr );
    }

    else {
        /** Set origin */
        cairo_move_to( cr, borderRect.x, borderRect.y );

        /** Top border */
        cairo_line_to( cr, borderRect.width, borderRect.y );

        /** Right border */
        cairo_line_to( cr, borderRect.width, borderRect.height );

        /** Bottom border */
        cairo_line_to( cr, borderRect.x,     borderRect.height );

        cairo_close_path( cr );
    }
}


/** Create a z theme with the default parameters */
wf::windecor::decoration_theme_t::decoration_theme_t( std::string app_id, bool isDialog, bool sticky ) {
    mAppId         = app_id;
    this->isDialog = isDialog;
    this->isSticky = sticky;
    themeMgr       = new IconThemeManager( iconTheme );
}


wf::windecor::decoration_theme_t::~decoration_theme_t() {
    delete themeMgr;
}


/** @return The available height for displaying the title */
int wf::windecor::decoration_theme_t::get_title_height() const {
    return title_height;
}


/** @return The available border for resizing */
int wf::windecor::decoration_theme_t::get_border_size() const {
    return border_size;
}


/*
 * * @return titlebar position
 ** left = 1, top = 2, right = 3, bottom = 4, notitlebar = 0
 */
int wf::windecor::decoration_theme_t::get_titlebar_position() const {
    return title_position;
}


void wf::windecor::decoration_theme_t::set_hover_button( int btn ) {
    hover_button = btn;
}


/**
 * Fill the given rectangle with the background color( s ).
 *
 * @param fb The target framebuffer, must have been bound already
 * @param rectangle The rectangle to redraw.
 * @param scissor The GL scissor rectangle to use.
 * @param active Whether to use active or inactive colors
 */
void wf::windecor::decoration_theme_t::render_background( const render_target_t& fb, geometry_t rectangle, const geometry_t& scissor, int state ) const {
    color_t color = (state > 0 ? (state == 1 ? active_color : attn_color) : inactive_color).value();

    int border_radius = (isTiled ? 0 : (title_position ? 5 : 0) );

    cairo_surface_t *surface = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, rectangle.width, rectangle.height );
    auto            cr       = cairo_create( surface );

    /** We want a smooth curve at the corner */
    cairo_set_antialias( cr, CAIRO_ANTIALIAS_BEST );

    /** Clear the titlebar background */
    cairo_set_operator( cr, CAIRO_OPERATOR_CLEAR );
    cairo_set_source_rgba( cr, 0, 0, 0, 0 );
    cairo_rectangle( cr, 0, 0, rectangle.width, rectangle.height );
    cairo_fill( cr );

    /** Draw the titlebar */
    cairo_set_operator( cr, CAIRO_OPERATOR_OVER );
    cairo_set_source_rgba( cr, color.r, color.g, color.b, color.a );

    drawRoundedRectangle( cr, rectangle, title_position, border_radius, (border_size.value() ? 1.0 : 0.0) );
    cairo_fill( cr );

    /**
     * If the user wants dynamic border and the border_size is greater than zero, draw it.
     * border_color is used of active views, inactive_color for inactive views, unless the
     * mouse is hovering on some button, in which case the border will take that color.
     */
    if ( border_size.value() and dynamic_border.value() ) {
        /** By default, we'll use the user defined border color */
        wf::color_t color = border_color.value();

        /** If using dynamic border colors, set the border color appropriately */
        switch ( hover_button ) {
            case BUTTON_CLOSE: {
                color = close_color.value();
                break;
            }

            case BUTTON_TOGGLE_MAXIMIZE: {
                color = maximize_color.value();
                break;
            }

            case BUTTON_MINIMIZE: {
                color = minimize_color.value();
                break;
            }

            case BUTTON_STICKY: {
                color = sticky_color.value();
                break;
            }

            default: {
                color = (state > 0 ? (state == 1 ? border_color : attn_color) : inactive_color).value();
                break;
            }
        }

        /** Set the line color */
        cairo_set_source_rgba( cr, color.r, color.g, color.b, 1.0 );
        /** We need a single thin line */
        cairo_set_line_width( cr, 1.0 );

        /** Draw the line */
        drawRoundedRectangle( cr, rectangle, title_position, border_radius, 1.0 );
        cairo_stroke( cr );
    }

    /** Destroy the context */
    cairo_destroy( cr );

    wf::simple_texture_t curve;

    cairo_surface_upload_to_texture( surface, curve );
    cairo_surface_destroy( surface );

    OpenGL::render_begin( fb );
    fb.logic_scissor( scissor );
    OpenGL::render_texture( curve.tex, fb, rectangle, glm::vec4( 1.0f ), OpenGL::TEXTURE_TRANSFORM_INVERT_Y );
    OpenGL::render_end();
}


/**
 * Render the given text on a cairo_surface_t with the given size.
 * The caller is responsible for freeing the memory afterwards.
 */
cairo_surface_t * wf::windecor::decoration_theme_t::render_text( std::string text, int width, int height ) const {
    /**
     * If we should not render a titlebar, return a nullptr.
     * This hould not matter, because we will not be rendering
     * the title in such a case.
     */
    if ( title_position == 0 ) {
        return nullptr;
    }

    /** Handle Left/Right titlebar */
    if ( (title_position % 2 == 1) and (height <= 20) ) {
        return nullptr;
    }

    /** Handle Top/Bottom titlebar */
    else if ( (title_position % 2 == 0) and (width <= 20) ) {
        return nullptr;
    }

    const auto      format = CAIRO_FORMAT_ARGB32;
    cairo_surface_t *surface;

    surface = cairo_image_surface_create( format, width, height );
    auto cr = cairo_create( surface );

    if ( cairo_status( cr ) != CAIRO_STATUS_SUCCESS ) {
        return nullptr;
    }

    std::string fname = font.value();
    double      fsize = ( double )font_size.value();
    wf::color_t frgba = font_color.value();

    // Font Color
    cairo_set_source_rgba( cr, frgba.r, frgba.g, frgba.b, frgba.a );

    PangoFontDescription *font_desc;
    PangoLayout          *layout;

    /** Initiualize font family and size */
    font_desc = pango_font_description_from_string( fname.c_str() );
    pango_font_description_set_absolute_size( font_desc, fsize * PANGO_SCALE );
    pango_font_description_set_gravity( font_desc, PANGO_GRAVITY_AUTO );

    /** Create Pango text layout */
    layout = pango_cairo_create_layout( cr );
    pango_layout_set_font_description( layout, font_desc );
    pango_layout_set_width( layout, (title_position % 2 == 1 ? height - 10 : width - 10) * PANGO_SCALE );

    switch ( title_align ) {
        case 0: {
            pango_layout_set_alignment( layout, PANGO_ALIGN_LEFT );
            break;
        }

        case 1: {
            pango_layout_set_alignment( layout, PANGO_ALIGN_CENTER );
            break;
        }

        case 2: {
            pango_layout_set_alignment( layout, PANGO_ALIGN_RIGHT );
            break;
        }

        default: {
            break;
        }
    }

    pango_layout_set_ellipsize( layout, PANGO_ELLIPSIZE_END );

    PangoContext *pCtxt = pango_layout_get_context( layout );

    pango_context_set_base_gravity( pCtxt, PANGO_GRAVITY_AUTO );

    /** Set the text */
    pango_layout_set_text( layout, text.c_str(), text.size() );

    PangoRectangle logical;

    pango_layout_get_extents( layout, nullptr, &logical );

    /** Handle Left/Right titlebar */
    if ( title_position % 2 == 1 ) {
        /**
         * Reposition before rotation
         */

        int offset = (width - logical.height / PANGO_SCALE) / 2;
        cairo_move_to( cr, offset, height - 5 );

        /** Rotate the surface */
        cairo_rotate( cr, -M_PI / 2.0 );
    }

    else {
        /** 10 px padding to the icon */
        int offset = (height - logical.height / PANGO_SCALE) / 2;
        cairo_move_to( cr, 5, offset );
    }

    /** Render the text */
    pango_cairo_show_layout( cr, layout );

    /** Release the font_description object */
    pango_font_description_free( font_desc );

    /** Release the layout */
    g_object_unref( layout );

    /** Release the cairo_context object */
    cairo_destroy( cr );

    /** Return the object */
    return surface;
}


cairo_surface_t * wf::windecor::decoration_theme_t::get_button_surface( button_type_t button, const button_state_t& state ) const {
    /** Let's create a button surface first */
    cairo_surface_t *button_surface = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, state.width, state.height );

    /** Then cairo context */
    auto cr = cairo_create( button_surface );

    /** Now, we enable antialiasing */
    cairo_set_antialias( cr, CAIRO_ANTIALIAS_BEST );

    /* Clear the button background */
    cairo_set_operator( cr, CAIRO_OPERATOR_CLEAR );
    cairo_set_source_rgba( cr, 0, 0, 0, 0 );
    cairo_rectangle( cr, 0, 0, state.width, state.height );
    cairo_fill( cr );

    /** Set the render mode to CAIRO_OPERATOR_OVER */
    cairo_set_operator( cr, CAIRO_OPERATOR_OVER );

    /** We should not render a BG for an icon */
    if ( button == BUTTON_ICON ) {
        cairo_set_source_rgba( cr, 0.0, 0.0, 0.0, 0.0 );
        cairo_rectangle( cr, 0.0, 0.0, state.width, state.height );
        cairo_fill( cr );

        /** Get the icon path */
        std::string iconPath = themeMgr->iconPathForAppId( mAppId );

        /** Icon surface */
        cairo_surface_t *button_icon;

        /* Draw svg using rsvg on the icon surface */
        if ( iconPath.find( ".svg" ) != std::string::npos ) {
            GFile      *svgFile = g_file_new_for_path( iconPath.c_str() );
            RsvgHandle *svg     = rsvg_handle_new_from_gfile_sync( svgFile, RSVG_HANDLE_FLAGS_NONE, NULL, NULL );

            button_icon = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, state.width, state.height );
            auto          crsvg = cairo_create( button_icon );
            RsvgRectangle rect{ 0, 0, state.width, state.height };
            rsvg_handle_render_document( svg, crsvg, &rect, nullptr );
            cairo_destroy( crsvg );

            g_object_unref( svg );
            g_object_unref( svgFile );
        }

        /* Draw png on the icon surface */
        else {
            button_icon = cairo_image_surface_create_from_png( iconPath.c_str() );
        }

        cairo_scale( cr,
                     1.0 * state.width / cairo_image_surface_get_width( button_icon ),
                     1.0 * state.height / cairo_image_surface_get_height( button_icon )
        );

        /** Render the icon on the button surface */
        cairo_set_source_surface( cr, button_icon, 0, 0 );
        cairo_paint( cr );

        cairo_destroy( cr );

        return button_surface;
    }

    /** A gray that looks good on light and dark themes */
    color_t base = { 0.60, 0.60, 0.63, 0.36 };

    /**
     * We just need the alpha component.
     * r == g == b == 0.0 will be directly set
     */
    double hover = 0.27;

    /**
     * Coloured base on hover/press. Don't compare float to 0
     * Also use a colored base if button_always_colored is true
     */
    if ( (fabs( state.hover_progress ) > 1e-3) or always_colored.value() ) {
        /** Choose the corect color for the correct button */
        switch ( button ) {
            case BUTTON_CLOSE: {
                base = close_color.value();
                break;
            }

            case BUTTON_TOGGLE_MAXIMIZE: {
                base = maximize_color.value();
                break;
            }

            case BUTTON_MINIMIZE: {
                base = minimize_color.value();
                break;
            }

            case BUTTON_STICKY: {
                base = sticky_color.value();
                break;
            }

            default: {
                assert( false );
            }
        }

        /** Draw the base when hovering */
        if ( state.hover_progress > 1e-3 ) {
            cairo_set_source_rgba(
                cr,
                base.r,
                base.g,
                base.b,
                base.a + hover * state.hover_progress
            );
        }

        /** Draw a colored base */
        else if ( always_colored.value() ) {
            cairo_set_source_rgba(
                cr,
                base.r,
                base.g,
                base.b,
                0.25
            );
        }

        cairo_arc( cr, state.width / 2, state.height / 2, state.width / 2, 0, 2 * M_PI );
        cairo_fill( cr );

        /** Draw the border only when we hover over the button or press it */
        cairo_set_line_width( cr, state.border );
        cairo_set_source_rgba( cr, 0.00, 0.00, 0.00, 1.5 * hover * state.hover_progress );

        // This renders great on my screen( 110 dpi 1376x768 lcd screen )
        // How this would appear on a Hi-DPI screen is questionable
        double r = state.width / 2 - 0.5 * state.border;
        cairo_arc( cr, state.width / 2, state.height / 2, r, 0, 2 * M_PI );
        cairo_stroke( cr );
    }

    else {
        if ( (button == BUTTON_STICKY) && isSticky ) {
            color_t base = sticky_color.value();

            /** Draw the base */
            cairo_set_source_rgba(
                cr,
                base.r,
                base.g,
                base.b,
                base.a + hover * state.hover_progress
            );
            cairo_arc( cr, state.width / 2, state.height / 2, state.width / 2, 0, 2 * M_PI );
            cairo_fill( cr );

            /** Draw a point at the center to indicate that the view is sticky */
            cairo_set_line_width( cr, state.border );
            cairo_arc( cr, state.width / 2, state.height / 2, 2, 0, 2 * M_PI );
            cairo_fill( cr );

            // This renders great on my screen( 110 dpi 1376x768 lcd screen )
            // How this would appear on a Hi-DPI screen is questionable
            double r = state.width / 2 - 0.5 * state.border;
            cairo_arc( cr, state.width / 2, state.height / 2, r, 0, 2 * M_PI );
            cairo_stroke( cr );
        }

        else {
            /** Draw a gray base */
            cairo_set_source_rgba( cr, base.r, base.g, base.b, base.a );
            cairo_arc( cr, state.width / 2, state.height / 2, state.width / 2, 0, 2 * M_PI );
            cairo_fill( cr );

            /** Draw a border */
            cairo_set_line_width( cr, state.border );
            cairo_set_source_rgba( cr, 0.00, 0.00, 0.00, 1.5 * hover );

            // This renders great on my screen( 110 dpi 1376x768 lcd screen )
            // How this would appear on a Hi-DPI screen is questionable
            double r = state.width / 2 - 0.5 * state.border;
            cairo_arc( cr, state.width / 2, state.height / 2, r, 0, 2 * M_PI );
            cairo_stroke( cr );
        }
    }

    cairo_destroy( cr );

    return button_surface;
}
